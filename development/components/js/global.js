/*========================*
 *           JS           *
 *========================*/

// import for others scripts to use
window.$ = $;
window.jQuery = jQuery;

import 'owl.carousel';
// import 'fullpage.js/vendors/scrolloverflow.min';
// import 'fullpage.js/dist/jquery.fullpage.min';
import 'jquery-scrollify/jquery.scrollify';

$(() => {




	/* ======= Global Actions ======= */
	const
		k = 'click',
		act = 'active-item',
		dis = 'hidden-item';
	/* ======= ---- ======= */

	/* ======= list ======= */
	const ___order__list_ = () => {
		pageMain.__load();
	};
	/* ======= ---- ======= */

	const pageMain = {
		__load() {
			let
				nameSlider = ['.js__bigsliderOwl'],
				nameSliderNav = ['.js__bigsliderOwlnav'],
				formID = '.js__reviewFormID',
				formSend = '.js__reviewFormSubmit',
				regexpEmail = /\S+[@]\S+[.]\w+$/gsi,
				modalFormPhone = '.js__modalForm [name="phone"]',
				captchaNumber = '';

			let setting = {
				// * Run functions once
				__firstRuns() {
					this.bigsliderOwl();
					this.headerSlim();
					this.reviewFormCaptcha();
					this.verticalScroll();
					this.verticalNavigation();
				},
				// * Handlers
				__handlers() {
					this.bigsliderOwlnavClick(nameSlider[0], nameSliderNav[0]);
					$(window).on('scroll', () => this.headerSlim() + this.scrollUPShow());
					$(window).on('scroll', () => this.verticalScrollCount());
					$(formSend).on(k, (event) => this.reviewFormResult(event) + this.reviewFormClick());
					$(formID).find('input[required]').on('keyup', (event) => this.reviewFormKeyupGlobal(event));
					$(modalFormPhone).on('focus keyup', (event) => this.modalFormMask(event));
					$('.js__modalSubmit').on(k, (event) => this.modalFormSubmit(event));
					$('.js__scrollUP').on(k, () => $.scrollify.move('#hash0'));
					$('.js__scrollDOWN').on(k, () => $.scrollify.next());
					$('.js__headerPhoneBtn, .js__orderCall').on(k, (event) => this.modalFormWindow(event, 1));
					$('.js__headerSelect').children().on(k, (event) => this.verticalScrollIndex(event));
					$('.js__headerMenu').on(k, (event) => this.headerMenu(event));
				},

				/* ======= Code ======= */
				
				scrollUPShow() {
					$(window).scrollTop() >= 100
						? $('.js__scrollUP').removeClass(act+'-out') + $('.js__scrollUP').addClass(act)
						: $('.js__scrollUP').addClass(act+'-out') + $('.js__scrollUP').removeClass(act);
				},

				headerMenu() {
					let z = () => $('.js__headerSlim').toggleClass('open-item');
					let x = () => $('.js__headerSelect').toggleClass('open-item');
					setTimeout(() => x(), 100, z());
				},

				verticalNavigation() {
					let spanItem = '<span class="vertical-nav__dot">&nbsp;</span>';
					let createItem = () => $('.js__verticalNav').append(`${spanItem}`);
					for (var i = 0; i < $('.js__vertical').length - 1; i++) {
						createItem();
					}
					$('.js__verticalNav').children().removeClass(act);
					$('.js__verticalNav').children().eq($('#vertical').attr('data-section-is')).addClass(act);
				},

				verticalScrollCount() {
					let
						navEl = '.js__verticalNav',
						scrollIndex = () => $.scrollify.currentIndex() - 1,
						selector = () => $('#vertical').attr('data-section-is'),
						currentNav = () => {
							$(navEl).children().removeClass(act);
							$(navEl).children().eq($('#vertical').attr('data-section-is')).addClass(act);
						};

					selector() === scrollIndex() ? '' : $('#vertical').attr('data-section-is', scrollIndex()) && '';
					$(navEl).children(`.${act}`).index() === scrollIndex() ? '' : currentNav();
				},

				verticalScrollIndex(event) {
					event.preventDefault();
					let pageIndex = () => $(event.currentTarget).attr('href');
					$.scrollify.move(pageIndex());
				},

				verticalScroll() {
					let
						sFull = 'js__fullscroll',
						sBase = 'js__basescroll',
						count = $('.js__vertical');

					for (let i = 0; i < count.length; i++) {
						count.eq(i).addClass(`${sFull}`);
					}

					$.scrollify({
						section: `.${sFull}`,
						sectionName: 'section-name',
						interstitialSection: '',
						easing: 'easeOutExpo',
						scrollSpeed: 1100,
						offset: -79,
						scrollbars: true,
						standardScrollElements: `.${sBase}`,
						setHeights: false,
						overflowScroll: true,
						updateHash: true,
						touchScroll: true,
						before: function() {},
						after: function() {},
						afterResize: function() {},
						afterRender: function() {},
					});
				},

				modalFormSubmit() {
					let mForm = $('.js__modalForm').find('input[required]');
					mForm.eq(0).val().length >= 1
						&& mForm.eq(1).val().length >= 1
						&& $('.js__modalForm [name="modal-call"]').is(':checked') === true
						? $('.js__modalSubmit').addClass('modal-form__button--success')
						&& setTimeout(() => $('.js__modalSubmit').text('Отправлено') + $('.js__modalClose').click(), 470, $('.js__modalSubmit').attr('disabled', 'disabled'))
						: '';
				},

				modalFormWindow(event, anima) {
					event.preventDefault();
					let
						hidden = 'hidden-item',
						cont = 'modal__container',
						bg = 'modal__section--background',
						mod = '.modal__section',
						closeCount = false,
						toggleHide = () => $(mod).toggleClass(hidden);

					let ani = (direct) => {
						$(`.${cont}`).toggleClass(`${cont}--animate-${direct}`);
						$(`.${bg}`).toggleClass(`${bg}-animate-${direct}`);
					};

					let animaIN = () => toggleHide() + ani('in');
					let animaOUT = () => setTimeout(() => toggleHide() + ani('out'), 600, ani('in') + ani('out'));
					let animeClose = () => {
						$('.js__missclick, .js__modalClose').off();
						closeCount = true && closeCount ? '' : animaOUT();
					};

					let closeEvent = () => {
						$('.js__modalClose, .js__missclick').one(k, () => animeClose());
						$(window).one('keyup keypress', (event) => event.keyCode === 27 ? animeClose() : '');
					};

					anima > 0
						? setTimeout(() => closeEvent(), 800, animaIN())
						: setTimeout(() => animaOUT());
				},

				modalFormMask(event) {
					let target = $(event.currentTarget);
					target.val(target.val().replace(/[^0-9]/gi, '').replace(/\s+/gi, ', '));
				},

				reviewFormKeyupEmailCaptcha(event) {
					let
						target = $(event.currentTarget),
						email = target.is('[name="email"]'),
						captcha = target.is('[name="captcha"]'),
						watch = (param) => target.attr('data-input', param);

					email
						? target.val().match(regexpEmail) ? watch('true') : watch('false')
						: '';
					captcha
						? target.val().length >= 5 && target.val().toUpperCase() === captchaNumber
						? watch('true') : watch('false')
						: '';
				},

				reviewFormKeyupGlobal(event) {
					let target = $(event.currentTarget);
					let watch = (param) => target.attr('data-input', param);

					target.val().length <= 3 || target.is('[name="email"]')
						? watch('false')
						: watch('true');

					this.reviewFormKeyupEmailCaptcha(event);
				},


				reviewFormClick() {
					let
						email = $(`${formID} input[name="email"]`),
						captcha = $(`${formID} input[name="captcha"]`),
						requiredElem = $(formID).find('input[required]'),
						inputRequare = (i) => requiredElem.eq(i).val().length,
						watch = (item, param) => item.attr('data-input', param);

					for (let i = 0; i < requiredElem.length; i++) {
						inputRequare(i) >= 3
							? requiredElem.eq(i).attr('data-input', 'true')
							: requiredElem.eq(i).attr('data-input', 'false');
					}

					email.val().match(regexpEmail)
						? watch(email, 'true')
						: watch(email, 'false');
					captcha.val().toUpperCase() === captchaNumber
						? watch(captcha, 'true')
						: watch(captcha, 'false');
				},

				reviewFormResult(event) {
					event.preventDefault();
					let
						r = 'input[required]',
						rd = 'input[required][data-input="true"]',
						readOnlyKill = () => $(formSend).removeAttr('disabled'),
						animateMe = (param) => $(formSend).attr('data-state', param),
						findRq = (input) => $(formID).find(input).length,
						readOnly = () => $(formSend).attr('disabled', 'true'),
						timeOut = () => setTimeout(() => animateMe('') + readOnlyKill(), 1200);

					findRq(rd) >= findRq(r)
						? readOnly() + animateMe('success')
						&& timeOut() + this.reviewFormDisabled()
						: readOnly() + animateMe('error')
						&& timeOut();

				},

				reviewFormDisabled() {
					$(formID).addClass('hidden-form');
					$('.review-form__before').addClass('active');
					$(formID).find('input, textarea').attr('disabled', 'disabled');
					$(formID).find('[type="submit"]').removeClass('js__reviewFormSubmit').attr('disabled', 'disabled');
				},

				reviewFormCaptcha() {
					let randomCaptcha = [...Array(5)].map(() => Math.random().toString(36)[3]).join('');
					captchaNumber = randomCaptcha.toUpperCase();

					// canvas
					let
						canvas = document.querySelector('.js__reviewFormRiddle'),
						ctx = canvas.getContext('2d');

					ctx.fillStyle = '#0c09ad';
					ctx.font = 'italic 2rem font-MyriadPro-Bold';
					ctx.font = 'bold 26px sans-serif';
					ctx.fillText(captchaNumber, 105, 75);

					///////////////////////
					// Hide in ShadowDom //
					///////////////////////
					// let host = document.querySelector('.js__reviewFormRiddle');
					// let shadowRoot = host.attachShadow({ mode: 'open' });
					// let sameDiv = document.createElement('span');
					// sameDiv.textContent = randomCaptcha;
					// shadowRoot.appendChild(sameDiv);

				},

				headerSlim() {
					let
						phone = $('.js__headerPhone'),
						slim = $('.js__headerSlim'),
						button = $('.js__headerPhoneBtn');

					let scrollButton = () => {
						$(window).scrollTop() >= 100
							? slim.addClass('header-nav__slim')
							&& phone.addClass(dis) + button.removeClass(dis)
							: slim.removeClass('header-nav__slim')
							&& phone.removeClass(dis) + button.addClass(dis);
					};

					$(window).width() >= 520 ? scrollButton() : '';

				},

				bigsliderOwlnavClick(name, nav) {
					$(`${nav} .js__owlRight`).on(k, () => $(name).trigger('next.owl.carousel'));
					$(`${nav} .js__owlLeft`).on(k, () => $(name).trigger('prev.owl.carousel', [300]));
					$('.js__bigsliderOwlnav .js__owlarrow').children().on(k, (event) => this.headerSelect(event));
				},

				bigsliderOwl() {
					$(nameSlider[0]).owlCarousel({
						loop: true,
						margin: 10,
						nav: false,
						dots: true,
						responsive: {
							0: { items: 1 },
							600: { items: 1 },
							1000: { items: 1 }
						}
					});
				},

				/* ======= ---- ======= */

				// * Loaders 
				__loaders() {
					this.__firstRuns();
					this.__handlers();
				}
			};
			setting.__loaders();
		}
	};

	/* ======= list ======= */
	___order__list_();
	/* ======= ---- ======= */

});