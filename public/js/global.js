webpackJsonp([0],[
/* 0 */,
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function($, jQuery, __webpack_provided_window_dot_jQuery) {

__webpack_require__(2);

__webpack_require__(3);

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

/*========================*
 *           JS           *
 *========================*/
// import for others scripts to use
window.$ = $;
__webpack_provided_window_dot_jQuery = jQuery;
$(function () {
  /* ======= Global Actions ======= */
  var k = 'click',
      act = 'active-item',
      dis = 'hidden-item';
  /* ======= ---- ======= */

  /* ======= list ======= */

  var ___order__list_ = function ___order__list_() {
    pageMain.__load();
  };
  /* ======= ---- ======= */


  var pageMain = {
    __load: function __load() {
      var nameSlider = ['.js__bigsliderOwl'],
          nameSliderNav = ['.js__bigsliderOwlnav'],
          formID = '.js__reviewFormID',
          formSend = '.js__reviewFormSubmit',
          regexpEmail = /[\0-\x08\x0E-\x1F!-\x9F\xA1-\u167F\u1681-\u1FFF\u200B-\u2027\u202A-\u202E\u2030-\u205E\u2060-\u2FFF\u3001-\uFEFE\uFF00-\uFFFF]+@[\0-\x08\x0E-\x1F!-\x9F\xA1-\u167F\u1681-\u1FFF\u200B-\u2027\u202A-\u202E\u2030-\u205E\u2060-\u2FFF\u3001-\uFEFE\uFF00-\uFFFF]+\.[0-9A-Z_a-z]+$/gi,
          modalFormPhone = '.js__modalForm [name="phone"]',
          captchaNumber = '';
      var setting = {
        // * Run functions once
        __firstRuns: function __firstRuns() {
          this.bigsliderOwl();
          this.headerSlim();
          this.reviewFormCaptcha();
          this.verticalScroll();
          this.verticalNavigation();
        },
        // * Handlers
        __handlers: function __handlers() {
          var _this = this;

          this.bigsliderOwlnavClick(nameSlider[0], nameSliderNav[0]);
          $(window).on('scroll', function () {
            return _this.headerSlim() + _this.scrollUPShow();
          });
          $(window).on('scroll', function () {
            return _this.verticalScrollCount();
          });
          $(formSend).on(k, function (event) {
            return _this.reviewFormResult(event) + _this.reviewFormClick();
          });
          $(formID).find('input[required]').on('keyup', function (event) {
            return _this.reviewFormKeyupGlobal(event);
          });
          $(modalFormPhone).on('focus keyup', function (event) {
            return _this.modalFormMask(event);
          });
          $('.js__modalSubmit').on(k, function (event) {
            return _this.modalFormSubmit(event);
          });
          $('.js__scrollUP').on(k, function () {
            return $.scrollify.move('#hash0');
          });
          $('.js__scrollDOWN').on(k, function () {
            return $.scrollify.next();
          });
          $('.js__headerPhoneBtn, .js__orderCall').on(k, function (event) {
            return _this.modalFormWindow(event, 1);
          });
          $('.js__headerSelect').children().on(k, function (event) {
            return _this.verticalScrollIndex(event);
          });
          $('.js__headerMenu').on(k, function (event) {
            return _this.headerMenu(event);
          });
        },

        /* ======= Code ======= */
        scrollUPShow: function scrollUPShow() {
          $(window).scrollTop() >= 100 ? $('.js__scrollUP').removeClass(act + '-out') + $('.js__scrollUP').addClass(act) : $('.js__scrollUP').addClass(act + '-out') + $('.js__scrollUP').removeClass(act);
        },
        headerMenu: function headerMenu() {
          var z = function z() {
            return $('.js__headerSlim').toggleClass('open-item');
          };

          var x = function x() {
            return $('.js__headerSelect').toggleClass('open-item');
          };

          setTimeout(function () {
            return x();
          }, 100, z());
        },
        verticalNavigation: function verticalNavigation() {
          var spanItem = '<span class="vertical-nav__dot">&nbsp;</span>';

          var createItem = function createItem() {
            return $('.js__verticalNav').append("".concat(spanItem));
          };

          for (var i = 0; i < $('.js__vertical').length - 1; i++) {
            createItem();
          }

          $('.js__verticalNav').children().removeClass(act);
          $('.js__verticalNav').children().eq($('#vertical').attr('data-section-is')).addClass(act);
        },
        verticalScrollCount: function verticalScrollCount() {
          var navEl = '.js__verticalNav',
              scrollIndex = function scrollIndex() {
            return $.scrollify.currentIndex() - 1;
          },
              selector = function selector() {
            return $('#vertical').attr('data-section-is');
          },
              currentNav = function currentNav() {
            $(navEl).children().removeClass(act);
            $(navEl).children().eq($('#vertical').attr('data-section-is')).addClass(act);
          };

          selector() === scrollIndex() ? '' : $('#vertical').attr('data-section-is', scrollIndex()) && '';
          $(navEl).children(".".concat(act)).index() === scrollIndex() ? '' : currentNav();
        },
        verticalScrollIndex: function verticalScrollIndex(event) {
          event.preventDefault();

          var pageIndex = function pageIndex() {
            return $(event.currentTarget).attr('href');
          };

          $.scrollify.move(pageIndex());
        },
        verticalScroll: function verticalScroll() {
          var sFull = 'js__fullscroll',
              sBase = 'js__basescroll',
              count = $('.js__vertical');

          for (var i = 0; i < count.length; i++) {
            count.eq(i).addClass("".concat(sFull));
          }

          $.scrollify({
            section: ".".concat(sFull),
            sectionName: 'section-name',
            interstitialSection: '',
            easing: 'easeOutExpo',
            scrollSpeed: 1100,
            offset: -79,
            scrollbars: true,
            standardScrollElements: ".".concat(sBase),
            setHeights: false,
            overflowScroll: true,
            updateHash: true,
            touchScroll: true,
            before: function before() {},
            after: function after() {},
            afterResize: function afterResize() {},
            afterRender: function afterRender() {}
          });
        },
        modalFormSubmit: function modalFormSubmit() {
          var mForm = $('.js__modalForm').find('input[required]');
          mForm.eq(0).val().length >= 1 && mForm.eq(1).val().length >= 1 && $('.js__modalForm [name="modal-call"]').is(':checked') === true ? $('.js__modalSubmit').addClass('modal-form__button--success') && setTimeout(function () {
            return $('.js__modalSubmit').text('Отправлено') + $('.js__modalClose').click();
          }, 470, $('.js__modalSubmit').attr('disabled', 'disabled')) : '';
        },
        modalFormWindow: function modalFormWindow(event, anima) {
          event.preventDefault();

          var hidden = 'hidden-item',
              cont = 'modal__container',
              bg = 'modal__section--background',
              mod = '.modal__section',
              closeCount = false,
              toggleHide = function toggleHide() {
            return $(mod).toggleClass(hidden);
          };

          var ani = function ani(direct) {
            $(".".concat(cont)).toggleClass("".concat(cont, "--animate-").concat(direct));
            $(".".concat(bg)).toggleClass("".concat(bg, "-animate-").concat(direct));
          };

          var animaIN = function animaIN() {
            return toggleHide() + ani('in');
          };

          var animaOUT = function animaOUT() {
            return setTimeout(function () {
              return toggleHide() + ani('out');
            }, 600, ani('in') + ani('out'));
          };

          var animeClose = function animeClose() {
            $('.js__missclick, .js__modalClose').off();
            closeCount = true && closeCount ? '' : animaOUT();
          };

          var closeEvent = function closeEvent() {
            $('.js__modalClose, .js__missclick').one(k, function () {
              return animeClose();
            });
            $(window).one('keyup keypress', function (event) {
              return event.keyCode === 27 ? animeClose() : '';
            });
          };

          anima > 0 ? setTimeout(function () {
            return closeEvent();
          }, 800, animaIN()) : setTimeout(function () {
            return animaOUT();
          });
        },
        modalFormMask: function modalFormMask(event) {
          var target = $(event.currentTarget);
          target.val(target.val().replace(/[^0-9]/gi, '').replace(/\s+/gi, ', '));
        },
        reviewFormKeyupEmailCaptcha: function reviewFormKeyupEmailCaptcha(event) {
          var target = $(event.currentTarget),
              email = target.is('[name="email"]'),
              captcha = target.is('[name="captcha"]'),
              watch = function watch(param) {
            return target.attr('data-input', param);
          };

          email ? target.val().match(regexpEmail) ? watch('true') : watch('false') : '';
          captcha ? target.val().length >= 5 && target.val().toUpperCase() === captchaNumber ? watch('true') : watch('false') : '';
        },
        reviewFormKeyupGlobal: function reviewFormKeyupGlobal(event) {
          var target = $(event.currentTarget);

          var watch = function watch(param) {
            return target.attr('data-input', param);
          };

          target.val().length <= 3 || target.is('[name="email"]') ? watch('false') : watch('true');
          this.reviewFormKeyupEmailCaptcha(event);
        },
        reviewFormClick: function reviewFormClick() {
          var email = $("".concat(formID, " input[name=\"email\"]")),
              captcha = $("".concat(formID, " input[name=\"captcha\"]")),
              requiredElem = $(formID).find('input[required]'),
              inputRequare = function inputRequare(i) {
            return requiredElem.eq(i).val().length;
          },
              watch = function watch(item, param) {
            return item.attr('data-input', param);
          };

          for (var i = 0; i < requiredElem.length; i++) {
            inputRequare(i) >= 3 ? requiredElem.eq(i).attr('data-input', 'true') : requiredElem.eq(i).attr('data-input', 'false');
          }

          email.val().match(regexpEmail) ? watch(email, 'true') : watch(email, 'false');
          captcha.val().toUpperCase() === captchaNumber ? watch(captcha, 'true') : watch(captcha, 'false');
        },
        reviewFormResult: function reviewFormResult(event) {
          event.preventDefault();

          var r = 'input[required]',
              rd = 'input[required][data-input="true"]',
              readOnlyKill = function readOnlyKill() {
            return $(formSend).removeAttr('disabled');
          },
              animateMe = function animateMe(param) {
            return $(formSend).attr('data-state', param);
          },
              findRq = function findRq(input) {
            return $(formID).find(input).length;
          },
              readOnly = function readOnly() {
            return $(formSend).attr('disabled', 'true');
          },
              timeOut = function timeOut() {
            return setTimeout(function () {
              return animateMe('') + readOnlyKill();
            }, 1200);
          };

          findRq(rd) >= findRq(r) ? readOnly() + animateMe('success') && timeOut() + this.reviewFormDisabled() : readOnly() + animateMe('error') && timeOut();
        },
        reviewFormDisabled: function reviewFormDisabled() {
          $(formID).addClass('hidden-form');
          $('.review-form__before').addClass('active');
          $(formID).find('input, textarea').attr('disabled', 'disabled');
          $(formID).find('[type="submit"]').removeClass('js__reviewFormSubmit').attr('disabled', 'disabled');
        },
        reviewFormCaptcha: function reviewFormCaptcha() {
          var randomCaptcha = _toConsumableArray(Array(5)).map(function () {
            return Math.random().toString(36)[3];
          }).join('');

          captchaNumber = randomCaptcha.toUpperCase(); // canvas

          var canvas = document.querySelector('.js__reviewFormRiddle'),
              ctx = canvas.getContext('2d');
          ctx.fillStyle = '#0c09ad';
          ctx.font = 'italic 2rem font-MyriadPro-Bold';
          ctx.font = 'bold 26px sans-serif';
          ctx.fillText(captchaNumber, 105, 75); ///////////////////////
          // Hide in ShadowDom //
          ///////////////////////
          // let host = document.querySelector('.js__reviewFormRiddle');
          // let shadowRoot = host.attachShadow({ mode: 'open' });
          // let sameDiv = document.createElement('span');
          // sameDiv.textContent = randomCaptcha;
          // shadowRoot.appendChild(sameDiv);
        },
        headerSlim: function headerSlim() {
          var phone = $('.js__headerPhone'),
              slim = $('.js__headerSlim'),
              button = $('.js__headerPhoneBtn');

          var scrollButton = function scrollButton() {
            $(window).scrollTop() >= 100 ? slim.addClass('header-nav__slim') && phone.addClass(dis) + button.removeClass(dis) : slim.removeClass('header-nav__slim') && phone.removeClass(dis) + button.addClass(dis);
          };

          $(window).width() >= 520 ? scrollButton() : '';
        },
        bigsliderOwlnavClick: function bigsliderOwlnavClick(name, nav) {
          var _this2 = this;

          $("".concat(nav, " .js__owlRight")).on(k, function () {
            return $(name).trigger('next.owl.carousel');
          });
          $("".concat(nav, " .js__owlLeft")).on(k, function () {
            return $(name).trigger('prev.owl.carousel', [300]);
          });
          $('.js__bigsliderOwlnav .js__owlarrow').children().on(k, function (event) {
            return _this2.headerSelect(event);
          });
        },
        bigsliderOwl: function bigsliderOwl() {
          $(nameSlider[0]).owlCarousel({
            loop: true,
            margin: 10,
            nav: false,
            dots: true,
            responsive: {
              0: {
                items: 1
              },
              600: {
                items: 1
              },
              1000: {
                items: 1
              }
            }
          });
        },

        /* ======= ---- ======= */
        // * Loaders 
        __loaders: function __loaders() {
          this.__firstRuns();

          this.__handlers();
        }
      };

      setting.__loaders();
    }
  };
  /* ======= list ======= */

  ___order__list_();
  /* ======= ---- ======= */

});
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(0), __webpack_require__(0), __webpack_require__(0)))

/***/ })
],[1]);